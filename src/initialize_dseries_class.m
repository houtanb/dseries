function initialize_dseries_class()

% Copyright (C) 2015-2019 Dynare Team
%
% This code is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare dseries submodule is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

% Get the path to the dseries toolbox.
dseries_src_root = strrep(which('initialize_dseries_class'),'initialize_dseries_class.m','');

% Check that the x13 binary is available
nox13 = false;
if ismac()
    if ~exist([dseries_src_root '../externals/x13/osx/64/x13as'], 'file')
        nox13 = true;
    end
elseif isunix()
    [status, ~] = system('which x13as');
    if status && ~(exist([dseries_src_root '../externals/x13/linux/64/x13as'], 'file') && exist([dseries_src_root '../externals/x13/linux/32/x13as'], 'file'))
        nox13 = true;
    end
elseif ispc()
    if ~(exist([dseries_src_root '../externals/x13/windows/64/x13as.exe'], 'file') && exist([dseries_src_root '../externals/x13/windows/32/x13as.exe'], 'file'))
        nox13 = true;
    end
else
    error('Unsupported platform.')
end
if nox13
    warning('X13 binary is not available.\nIf you are under Debian or Ubuntu, you can install it through your package manager, with ''apt install x13as''.\nOtherwise, you can install it manually by running\nthe function installx13() available in:\n\n\t%s\n', [dseries_src_root(1:end-4), 'externals' filesep() 'x13']);
end

% Set the subfolders to be added in the path.
p = {'read'; ...
     'utilities/is'; ...
     'utilities/op'; ...
     'utilities/convert'; ...
     'utilities/str'; ...
     'utilities/insert'; ...
     'utilities/file'; ...
     'utilities/from'; ...
     'utilities/print'; ...
     'utilities/variables'; ...
     'utilities/cumulate'; ...
     'utilities/struct'};

% Add /utilities/x13' if x13 binary is available.
if ~nox13
    p{end+1} = 'utilities/x13';
end

% Add missing routines if dynare is not in the path
if ~exist('isint','file')
    p{end+1} = 'utilities/missing/isint';
end

if ~exist('isoctave','file')
    p{end+1} = 'utilities/missing/isoctave';
end

if ~exist('shiftS','file')
    p{end+1} = 'utilities/missing/shiftS';
end

if ~exist('matlab_ver_less_than','file')
    p{end+1} = 'utilities/missing/matlab_ver_less_than';
end

if ~exist('demean','file')
    p{end+1} = 'utilities/missing/demean';
end

if ~exist('ndim','file')
    p{end+1} = 'utilities/missing/ndim';
end

if ~exist('OCTAVE_VERSION') && (~exist('rows','file') || ~exist('columns','file'))
    p{end+1} = 'utilities/missing/dims';
end

if ~exist('sample_hp_filter','file')
    p{end+1} = 'utilities/missing/sample_hp_filter';
end

if ~exist('get_file_extension','file')
    p{end+1} = 'utilities/missing/get_file_extension';
end

if exist('OCTAVE_VERSION') && ~exist('user_has_octave_forge_package','file')
    p{end+1} = 'utilities/missing/user_has_octave_forge_package';
end

if ~exist('get_cells_id','file')
    p{end+1} = 'utilities/missing/get_cells_id';
end

if ~exist('randomstring','file')
    p{end+1} = 'utilities/missing/randomstring';
end

if ~exist('one_sided_hp_filter','file')
    p{end+1} = 'utilities/missing/one_sided_hp_filter';
end

if ~exist('OCTAVE_VERSION') && ~exist('ismatrix','builtin')
    p{end+1} = 'utilities/missing/ismatrix';
end

if ~exist('OCTAVE_VERSION') && ~exist('isrow','builtin')
    p{end+1} = 'utilities/missing/isrow';
end

if ~exist('OCTAVE_VERSION') && ~exist('iscolumn','builtin')
    p{end+1} = 'utilities/missing/iscolumn';
end

if ~exist('OCTAVE_VERSION') && ~exist('strsplit','file')
    p{end+1} = 'utilities/missing/strsplit';
end

if ~exist('OCTAVE_VERSION') && ~exist('strjoin','file')
    p{end+1} = 'utilities/missing/strjoin';
end

% Set path
P = cellfun(@(c)[dseries_src_root c], p, 'uni', false);
addpath(P{:});

assignin('caller', 'dseries_src_root', dseries_src_root);
