function b = isstringdate(str)  % --*-- Unitary tests --*--

% Tests if the input string can be interpreted as a date.
%
% INPUTS
%  o str     string.
%
% OUTPUTS
%  o b       integer scalar, equal to 1 if str can be interpreted as a date or 0 otherwise.

% Copyright (C) 2013-2017 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

if ischar(str)
    b = isquarterly(str) || isyearly(str) || ismonthly(str) || isweekly(str);
else
    b = false;
end

%@test:1
%$
%$ date_1 = '1950M2';
%$ date_2 = '1950m2';
%$ date_3 = '-1950m2';
%$ date_4 = '1950m52';
%$ date_5 = ' 1950';
%$ date_6 = '1950Y';
%$ date_7 = '-1950a';
%$ date_8 = '1950m ';
%$ date_9 = 'A';
%$ date_10 = '1938Q';
%$ date_11 = 'Q4';
%$ date_12 = '1938M';
%$ date_13 = 'M11';
%$ date_14 = '1W';
%$ date_15 = 'W1';
%$
%$ t(1) = dassert(isstringdate(date_1),true);
%$ t(2) = dassert(isstringdate(date_2),true);
%$ t(3) = dassert(isstringdate(date_3),true);
%$ t(4) = dassert(isstringdate(date_4),false);
%$ t(5) = dassert(isstringdate(date_5),false);
%$ t(6) = dassert(isstringdate(date_6),true);
%$ t(7) = dassert(isstringdate(date_7),true);
%$ t(8) = dassert(isstringdate(date_8),false);
%$ t(9) = dassert(isstringdate(date_9),false);
%$ t(10) = dassert(isstringdate(date_10),false);
%$ t(11) = dassert(isstringdate(date_11),false);
%$ t(12) = dassert(isstringdate(date_12),false);
%$ t(13) = dassert(isstringdate(date_13),false);
%$ t(14) = dassert(isstringdate(date_14),false);
%$ t(15) = dassert(isstringdate(date_15),false);
%$
%$ T = all(t);
%@eof:1